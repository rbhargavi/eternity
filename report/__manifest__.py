# -*- coding: utf-8 -*-
{
    'name': 'Sale order',
    'version': '11.0.1.0.0',
    'summary': 'Sale Order',
    'description': """
      
        """,
    'category': 'Sales',
    'author': "Odoo",
    'company': 'Odoos',
    'maintainer': 'Odoo',
    'website': "https://www.odoo.com",
    'depends': [
        'base','sale','account','product','purchase','tax_summary'
    ],
    'data': [
#        'security/ir.model.access.csv',
#        'security/security.xml',
#        'views/hr_loan_seq.xml',
#        'data/salary_rule_loan.xml',
        'views/sale_report.xml',
        'views/sale_report_template.xml',
        'views/sale_extended_layout.xml',
        'views/sale_order.xml',

        'views/custom_header.xml',
        # 'views/custom_purchase_order_view.xml',
        'views/purchase_order_report.xml',
        'views/purchase_order_report_view.xml',
        'views/purchase_custom_view.xml',

        'views/invoice_report.xml',
        'views/invoice_template.xml',
        'views/account_invoice.xml',
        'views/company.xml',

        'views/quotation_custom_header.xml',
        'views/quotation_page_format.xml',
        'views/quotation_views.xml',
        

#        'views/hr_payroll.xml',
    ],
    'demo': [],
    'images': ['static/description/banner.png'],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': False,
}
