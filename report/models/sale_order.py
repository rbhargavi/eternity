from odoo import models, fields, api, _
from datetime import datetime



class SaleOrder(models.Model):
    _inherit  = 'sale.order'
    _description = "Sale Order"


    purchase_order_no = fields.Char(string="Purchase No")
    purchase_order_date = fields.Datetime(string="Purchase Date")
    round_off = fields.Float(compute='compute_round_off')
    custom_delivery_period=fields.Selection([('1 week','1 Week'),('2-3 weeks','2-3 Weeks'),('1 month','1 Month')],string= "Delivery Peroid")
    warranty=fields.Selection([('1 year','1 Year'),('2 year','2 Year')],string="Warranty Period:")
    packing_detail=fields.Char(string="Packing:")
    validity_detail=fields.Char(string="Validity:")
    custom_quotation_date=fields.Date(string="Quotation Date:", default=datetime.today())
    custom_buyer_inquiry_date=fields.Date(string="Inquiry Date:", default=datetime.today())
    kind_attention = fields.Many2one('res.partner', string="Kind Attention")





    @api.depends('amount_untaxed','amount_tax')
    def compute_round_off(self):
        total=0.0
        total+=(self.amount_untaxed+self.amount_tax)
        total_1=0.0
        total_1+=round(total,0)
        round_1 = 0.0
        round_1 += (total_1 - total)
        if round_1<0.5 :
            n=0.0
            n+=round_1
            n_2=0.0
            n_2+=abs(n)
            self.round_off = n_2
        else:
            self.round_off = round_1
       





    @api.multi
    def get_extra_rows_1(self,lines):
        print("-------------",lines)
        if lines< 9:
            counter=lines
            rows=""
            while counter!=9:
                rows+="<tr>"
                for each in range(0,6):
                    rows+="<td style='border:1px solid;height:35px;border-top:none;border-bottom:none; border-color: black;'></td>"
                rows+="</tr>"
                counter +=1
            return rows

    @api.multi
    def get_extra_rows(self, lines):
        print("-------------", lines)
        if lines < 9:
            counter = lines
            rows = ""
            while counter != 9:
                rows += "<tr>"
                for each in range(0, 6):
                    rows += "<td style='border:1px solid gray;height:35px;border-top:none;border-bottom:none; border-color: gray;'></td>"
                rows += "</tr>"
                counter += 1
            return rows

    @api.depends('amount_total')
    def compute_text(self):
        return self.currency_id.amount_to_text(round(self.amount_total, 0))


class ResPartner(models.Model):
    _inherit  = 'res.partner'   

    customer_pan_no = fields.Char(string="Pan No")

class Product(models.Model):
    _inherit  = 'product.template'   

    frieght_charges = fields.Boolean(string="Frieght Charges")
    