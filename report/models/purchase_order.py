# -*- coding: utf-8 -*-
from odoo import api, fields, models,_
from num2words import num2words






class PurchaseOrder(models.Model):

    _inherit='purchase.order'

    round_off = fields.Float(compute='compute_round_off')
    kind_attention=fields.Many2one('res.partner', string="Kind Attention")
    delivery_date=fields.Date(string="Delivery Date")


    @api.depends('amount_untaxed', 'amount_tax')
    def compute_round_off(self):
        total = 0.0
        total += (self.amount_untaxed + self.amount_tax)
        total_1 = 0.0
        total_1 += round(total, 0)
        round_1 = 0.0
        round_1 += (total_1 - total)
        if round_1 < 0.5:
            n = 0.0
            n += round_1
            n_2 = 0.0
            n_2 += abs(n)
            self.round_off = n_2
        else:
            self.round_off = round_1


    @api.multi
    def get_extra_rows(self, lines):
        print("-------------", lines)
        if lines < 10:
            counter = lines
            rows = ""
            while counter != 10:
                rows += "<tr>"
                for each in range(0, 6):
                    rows += "<td style='border:1px solid black; height:35px;border-top:none; border-bottom:none; border-color: black; padding:15px'></td>"
                rows += "</tr>"
                counter += 1
            return rows






    @api.depends('amount_total')
    def compute_text(self):
        return self.currency_id.amount_to_text(round(self.amount_total,0))






# class CustomEternityPage(models.Model):
#
#     _inherit="res.company"
#
#     company_pan_card_id=fields.Char(string='PAN Card No.')
#
#
#
# class CustomVendorPage(models.Model):
#
#     _inherit="res.partner"
#
#
#     vendor_pan_card_id=fields.Char(string='PAN Card No.')



# class CustomVendorPage1(models.Model):
#
#
#     _inherit = "res.conutry.state"









