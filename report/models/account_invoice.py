from odoo import models, fields, api, _


class AccountInvoice(models.Model):
    _inherit  = 'account.invoice'
    _description = "Account Invoice"

    purchase_invoice_date = fields.Datetime(string="Purchase Date")
    purchase_invoice_no = fields.Char(string="Purchase No")
    purchase_round_off = fields.Float(compute='compute_round_off')
    kind_attention = fields.Many2one('res.partner', string="Kind Attention")



    # @api.depends('amount_untaxed','amount_tax')
    # def compute_round_off(self):
    #     total=0.0
    #     total+=(self.amount_untaxed+self.amount_tax)
    #     # print("totallughli",total)
    #     total_1=0.0
    #     total_1+=round(total,0)
    #     # print("hvhkhfkugku",total_1)
    #     round_1=0.0
    #     round_1+=(total_1-total)
    #     self.purchase_round_off= round_1

    
    @api.depends('amount_untaxed','amount_tax')
    def compute_round_off(self):
        total=0.0
        total+=(self.amount_untaxed+self.amount_tax)
        total_1=0.0
        total_1+=round(total,0)
        round_1 = 0.0
        round_1 += (total_1 - total)
        if round_1<0.5 :
            n=0.0
            n+=round_1
            n_2=0.0
            n_2+=abs(n)
            self.purchase_round_off = n_2
        else:
            self.purchase_round_off = round_1




    @api.multi
    def get_extra_rows(self,lines):
        print("-------------",lines)
        if lines< 7:
            counter=lines
            rows=""
            while counter!=7:
                rows+="<tr>"
                for each in range(0,6):
                    rows+="<td style='border:1px solid;height:35px;border-top:none;border-bottom:none; border-color: gray;'></td>"
                rows+="</tr>"
                counter +=1
            return rows

    @api.depends('amount_total')
    def compute_text(self):
        return self.currency_id.amount_to_text(round(self.amount_total, 0))


class ResPartner(models.Model):
    _inherit  = 'res.partner'   

    customer_pan_no = fields.Char(string="Pan No")

class Product(models.Model):
    _inherit  = 'product.template'   

    frieght_charges = fields.Boolean(string="Frieght Charges")
    