
from odoo import models, fields, api, _

class ResCompany(models.Model):
    _inherit  = 'res.company'   
    _description = "Res Company"


    comp_pan_no = fields.Char(string="Pan No")
    signature = fields.Binary(string="Signature")
    bank_name = fields.Char(string="Bank Name")
    ac_no = fields.Char(string="Ac no")
    branch = fields.Char(string="Branch")
    ifsc_code = fields.Char(string="IFSC code")
    city = fields.Char(string="City")
    zip_bank = fields.Char(string="Zip")
    terms_and_condition = fields.Html(string="Terms & Condition")
    eway_bill_no = fields.Char(string="E Way Bill No")
    eway_bill_date = fields.Date(string="E Way Bill Date")
    vehicle = fields.Char(string="Vehicle Number")
    recipt_no = fields.Char(string="Lorry Receipt No")
    custom_transit_id=fields.Char(string="Transit Insurance :")