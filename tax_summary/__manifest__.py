# -*- coding: utf-8 -*-
##############################################################################
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published
#    by the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see http://www.gnu.org/licenses/.
#
##############################################################################

{
    "name": "Taxes summary in Sale And Purchase Order",
    "version": "1.0",
    "license": "AGPL-3",
    "description": "Add new field in Sale and Purchase Order it can be display order line tax details in group",
    "depends":  [
                    "sale",
                    "account",
                    "purchase",

                ],
    "author": "Odoo",
    "category": "Sales",
    "website": "http://www.odoo.com/",
    "data": [
        "views/sale_order_views.xml",
        "views/purchase_views.xml",
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
