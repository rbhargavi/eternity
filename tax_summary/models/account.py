from openerp import models, fields, api,_
from odoo.exceptions import Warning

class AccountTax(models.Model):
    _inherit = 'account.tax'
    _order = 'name'
    
    def get_grouping_key1(self, tax_val):
        """ Returns a string that will be used to group 'sale.order.tax' or 'purchase.order.tax' sharing the same properties"""
        self.ensure_one()
        return str(tax_val['tax_id']) + '-' + str(tax_val['account_id'])


    @api.multi
    def unlink(self):
       	tax_id=self.env['purchase.order.line'].search([('taxes_id','in',self.id)])
       	if tax_id:
       		raise Warning(_('You trying to delete a record that is still referenced!'))
       	tax_id=self.env['sale.order.line'].search([('tax_id','in',self.id)])
       	if tax_id:
       		raise Warning(_('You trying to delete a record that is still referenced!'))
       	tax_id=self.env['account.invoice.line'].search([('invoice_line_tax_ids','in',self.id)])
       	if tax_id:
       		raise Warning(_('You trying to delete a record that is still referenced!'))
        return super(AccountTax, self).unlink()
    
AccountTax()