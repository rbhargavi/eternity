from openerp import models, fields, api
import openerp.addons.decimal_precision as dp

class SaleOrderTax(models.Model):
    _name = 'sale.order.tax'
    _table = 'sale_order_tax2'
    _order = 'name'
    sale_order = fields.Many2one('sale.order',
                                 string='Sale Order', ondelete='cascade')
    name = fields.Char(string='Tax Description', required=True)
    amount = fields.Float(string='Amount', digits=dp.get_precision('Sale'))
    account_id = fields.Many2one('account.account',
                                  string='Tax Account')
    tax_id=fields.Many2one('account.tax')
    sequence = fields.Integer(help="Gives the sequence order when displaying a list of invoice tax.")


class SaleOrder(models.Model):
    _inherit = 'sale.order'

    taxes = fields.One2many('sale.order.tax','sale_order', string='Taxes', copy=True)
    
    @api.onchange('order_line')
    def onchange_order_line(self):
        taxes_grouped= self.get_taxes_values()
        print('onchange-----',taxes_grouped)
        tax_lines = self.taxes.browse([])
        for tax in taxes_grouped.values():
            tax_lines += tax_lines.new(tax)
        self.taxes = tax_lines
        print('self.taxes-------------',self.taxes)
        return
    
#    tax add in order line return group of tax
    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        for line in self.order_line:
            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
#            compute all  taxes in order line 
            taxes = line.tax_id.compute_all(price_unit, self.currency_id, line.product_uom_qty, line.product_id, self.partner_id)['taxes']
            print('taxes==-===========',taxes)
            for tax in taxes:
                val = {
                    'order_id': self.id,
                    'name': tax['name'],
                    'tax_id': tax['id'],
                    'amount': tax['amount'],
                    'manual': False,
                    'sequence': tax['sequence'],
                    'account_analytic_id': tax['analytic'] and line.account_analytic_id.id or False,
                    'account_id': tax['account_id'],
                }
#                create browse object account.tax and call group function 
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key1(val)
                if key not in tax_grouped:
                    tax_grouped[key] = val
                    # print'tax_grouped----------',tax_grouped
                    print('tax_grouped--sorted--------',sorted(tax_grouped))
                else:
#                    amount increment  taxes one2many field 
                    tax_grouped[key]['amount'] += val['amount']
            # tax_grouped=sorted(tax_grouped.items(), key=lambda l: l[0].sequence)
            # tax_grouped = map(lambda l: (l[0].name, l[1]), tax_grouped)
        return tax_grouped
    
    
    
SaleOrder()