from openerp import models, fields, api
import openerp.addons.decimal_precision as dp


class PurchaseOrderTax(models.Model):
    _name = 'purchase.order.tax'
    _table = 'purchase_order_tax2'
    _order = 'name'
    
    order_id = fields.Many2one('purchase.order',
                                 string='Purchase Order', ondelete='cascade')
    name = fields.Char(string='Tax Description', required=True)
    amount = fields.Float(string='Amount', digits=dp.get_precision('Purchase'))
    account_id = fields.Many2one('account.account',
                                  string='Tax Account')



class PurchaseOrder(models.Model):
    _inherit = 'purchase.order'

    tax_ids = fields.One2many('purchase.order.tax','order_id', string='Taxes', copy=True)
    
    @api.onchange('order_line')
    def onchange_order_line(self):
        taxes_grouped = self.get_taxes_values()
        tax_lines = self.tax_ids.browse([])
        for tax in taxes_grouped.values():
            tax_lines += tax_lines.new(tax)
        self.tax_ids = tax_lines
        return

#        tax add in order line return group of tax
    @api.multi
    def get_taxes_values(self):
        tax_grouped = {}
        for line in self.order_line:
#            discount field add im order line use this line
#            price_unit = line.price_unit * (1 - (line.discount or 0.0) / 100.0)
#            compute all  taxes in order line 
            taxes = line.taxes_id.compute_all(line.price_unit, self.currency_id, line.product_qty, line.product_id, self.partner_id)['taxes']
            for tax in taxes:
                val = {
                    'order_id': self.id,
                    'name': tax['name'],
                    'tax_id': tax['id'],
                    'amount': tax['amount'],
                    'manual': False,
                    'sequence': tax['sequence'],
                    'account_analytic_id': tax['analytic'] and line.account_analytic_id.id or False,
                    'account_id': tax['account_id'],
                }
#                create browse object account.tax and call group function
                key = self.env['account.tax'].browse(tax['id']).get_grouping_key1(val)
                if key not in tax_grouped:
                    tax_grouped[key] = val
                else:
#                    amount increment  taxes one2many field 
                    tax_grouped[key]['amount'] += val['amount']
        return tax_grouped
    
PurchaseOrder()