from odoo import models, fields, api, _
from datetime import datetime



class SaleOrder(models.Model):
    _inherit  = 'sale.order'
    _description = "Sale Order"



    @api.onchange('partner_id')
    def onchange_partner_id(self):
        domain ={}
        res= super(SaleOrder,self).onchange_partner_id()
        if not res:
            res={}
        user_id=self.env['res.users'].browse(self.env.user.id)
        if user_id:
            browse_obj = user_id.user_has_groups('sales_team.group_sale_manager')
            if browse_obj:
                partner_ids = self.env['res.partner'].search([])
                domain = {'partner_id':  [('id', 'in', [ partner.id for partner in partner_ids])]}
            else:
                partner_ids = self.env['res.partner'].search([('user_id','=',user_id.id)])
                domain = {'partner_id':  [('id', 'in', [ partner.id for partner in partner_ids])]}
            if domain:
                res["domain"] = domain
        return res



   




