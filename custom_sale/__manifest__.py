# -*- coding: utf-8 -*-
{
    'name': 'Sale order eternity',
    'version': '11.0.1.0.0',
    'summary': 'Sale Order',
    'description': """
      
        """,
    'category': 'Sales',
    'author': "Odoo",
    'company': 'Odoo',
    'maintainer': 'Odoo',
    'website': "https://www.odoo.com",
    'depends': [
        'base','sale','account','product','purchase','tax_summary'
    ],
    'data': [
#
        

#        'views/hr_payroll.xml',
    ],
    'demo': [],
    'images': ['static/description/banner.png'],
    'license': 'AGPL-3',
    'installable': True,
    'auto_install': False,
    'application': False,
}
