# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Quotations/Sales Orders Approval',
    'version': '1.0',
    'category': 'Sales',
    'sequence': 15,
    'summary': 'Quotations/Sales Orders Approval',
    'description': """
Manage sales quotations and orders Approval.
    """,
    'author': 'Odoo',
    'website': 'https://www.odoo.com',
    'license': 'LGPL-3',
    'support': 'Odoo',
    'depends': ['base_setup','sale', 'sales_team','crm'],
    'data': [
        'report/schedule_template.xml',
        'security/security_sale.xml',
        'views/res_user_views.xml',
        'views/sale_view.xml',
        'views/sale_shedule.xml'
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
    'images': ['static/description/banner.png'],
}
