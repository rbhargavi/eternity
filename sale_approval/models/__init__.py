# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

from. import res_users
from .import sale
from .import custom_account_invoice
from .import sale_shedule
