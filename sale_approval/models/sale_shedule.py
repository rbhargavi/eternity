# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from datetime import date, datetime
import datetime


class CustomMail(models.Model):
    _inherit='mail.activity'

    date_deadline = fields.Datetime('Due Date', index=True, required=True, default=fields.Datetime.now)


class MailActivity(models.AbstractModel):
    _inherit = 'mail.activity.mixin'

    activity_date_deadline = fields.Datetime(
        'Next Activity Deadline', related='activity_ids.date_deadline',
        readonly=True, store=True,  # store to enable ordering + search
        groups="base.group_user")


class ResUsers(models.Model):
    _inherit='res.users'

    def get_today_date(self):
        today_date = date.today()
        return today_date


    @api.model
    def send_activity(self):
        sale_user_id = self.search([('sale_activity', '=', True)])
        if sale_user_id:
            print('------------------',sale_user_id)
            for sale in sale_user_id:
                # final_dict = {}
                # activity_id = self.env['mail.activity'].search([('user_id', '=', sale.id), ('date_deadline', '=', fields.date.today())])
                # print("==================================",activity_id)
                # over_due_activity_id = self.env['mail.activity'].search([('user_id', '=', sale.id), ('date_deadline', '<', fields.date.today())])
                # print("==================================", over_due_activity_id)
                # final_dict.update({'activity_id':activity_id,'over_due_activity_id':over_due_activity_id})
                activity_sale = self.send_activity_mail(sale)
                if activity_sale:
                    template = self.env.ref('sale_approval.new_sale_template_activity', False)
                    print("wdsddddddddddddddd", template)
                    if template:
                        email_to = sale.email
                        print("============",email_to)
                        model = 'res.users'
                        template.send_mail(sale.id, force_send=True,
                                           email_values={'email_to': email_to,
                                                         'model': model})



    @api.model
    def send_activity_mail(self,user):
        # user=self
        print("Userrrrrrrrrrrr",user)

        # error
        overdue_activity, today_activity = [], []
        final_dict = {}
        current_date=fields.Date.today()
        print("current_datecurrent_datecurrent_datecurrent_date",current_date, current_date+' 00:00:01')
        # self.env.cr.execute('select * from mail_activity where user_id = %s and date_deadline > %s group by activity_type_id, id',(user.id, current_date +" 00:00:01"))
        # activity_ids=[x[0] for x in self.env.cr.fetchall()]
        # print("activity_idsactivity_idsactivity_idsactivity_ids-------------",activity_ids)
        activity_id = self.env['mail.activity'].search([('user_id', '=', user.id), ('date_deadline', '>', current_date+' 00:00:01' )])
        final_dict['total_today']=len(activity_id)

        for activity in activity_id:
            d2 = date.today()
            d1 = datetime.datetime.strptime(activity.date_deadline, "%Y-%m-%d %H:%M:%S" ).date()
            today_date_day = (d2 - d1).days
            time = datetime.datetime.strptime(activity.date_deadline, "%Y-%m-%d %H:%M:%S").time()
            hours = time.strftime("%H:%M")
            today_vals = {'type':'todays activity','activity_type':activity.activity_type_id.name ,'summary':activity.summary,
                         'date':today_date_day,'time':hours
            }
            today_activity.append(today_vals)
        final_dict['today_details'] = today_activity
        over_due_activity_id = self.env['mail.activity'].search([('user_id', '=', user.id), ('date_deadline', '<', fields.Datetime.now())])
        final_dict['overdue_total'] = len(over_due_activity_id)
        if not activity_id and not over_due_activity_id:
            return False
        for activity in over_due_activity_id:
            d2 = date.today()
            d1 = datetime.datetime.strptime(activity.date_deadline, "%Y-%m-%d %H:%M:%S" ).date()
            overdue_date_day = (d2 - d1).days
            overdue_vals = {'type':'overdue activity','activity_type': activity.activity_type_id.name, 'summary': activity.summary,
                            'date':overdue_date_day
                    }
            overdue_activity.append(overdue_vals)
        final_dict['over_due_details'] = overdue_activity
        return final_dict

