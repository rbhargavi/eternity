# -*- coding: utf-8 -*-
from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT

class SaleOrder(models.Model):
    _inherit = "sale.order"
     
    state = fields.Selection([
        ('draft', 'Quotation'),
        ('sent', 'Quotation Sent'),
        ('waiting_for_approval', 'Waiting For Approval'),
        ('sale', 'Sales Order'),
        ('done', 'Locked'),
        ('cancel', 'Cancelled'),
        ], string='Status', readonly=True, copy=False, index=True, track_visibility='onchange', default='draft')
    discount_notes = fields.Float('Discount Note')
    next_discount_amount = fields.Float('Next Discount Amount')
    po_no = fields.Char(string="PO NO")
    po_date = fields.Date(string="PO Date")
    boolean_field = fields.Boolean(string="Approve Field")


    @api.multi
    def action_confirm(self):
        res= super(SaleOrder, self).action_confirm()
        if self.boolean_field == False:
            order_id=self.order_line.filtered(lambda l: l.product_id.product_tmpl_id.list_price != l.price_unit)
            if order_id:
                raise UserError(_(
                    'You need to approve before confirm the sale'))
        return res

    @api.multi
    def approve_quoatation(self):
       self.write({'state':'draft','boolean_field':True})

    @api.multi
    def action_sales_approval(self):
        self.write({'state': 'waiting_for_approval'})


    
    @api.multi
    def get_discount(self):
        return self.env.context.get('discount_percentage', 0)
    
    @api.multi
    def get_reason_notes(self):
        return self.env.context.get('discount_notes', '')

    @api.multi
    def get_reason_note(self):
        return self.env.context.get('discount_notes', '')
    
    @api.multi
    def escalate_order(self):
        self.ensure_one()
        template = self.env['ir.model.data'].get_object('sale_approval', 'email_template_sale_approval_mail')
        self.env['mail.template'].browse(template.id).send_mail(self.id,force_send=True)
        return True
  
class SaleOrderLine(models.Model):
    _inherit = 'sale.order.line'
     
    @api.onchange('discount')
    def onchang_discount_validate(self):
        if self.discount:
            approver_id = self.order_id.approver_id
            if not self.discount <= approver_id.sale_order_discount_limit:
                value = {
                    'discount': 00.0
                }
                warning = {
                    'title': _('Warning!'),
                    'message' : (_('Your discount limit is lesser than given discount.!'))
                }
                return {'warning': warning, 'value': value}


 

