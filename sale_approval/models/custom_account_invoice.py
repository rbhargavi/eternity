from odoo import api, fields, models, _
from odoo.exceptions import UserError
from odoo.tools import float_is_zero, float_compare, DEFAULT_SERVER_DATETIME_FORMAT

class CustomActionInvoice(models.Model):
    _inherit = "account.invoice"

    @api.multi
    def action_invoice_open(self):

        browse_obj = self.env['stock.picking'].search([('origin','=',self.origin)])
        print('================',browse_obj)
        for line in self.invoice_line_ids:
            order_id = browse_obj.move_lines.filtered(lambda l: l.product_id.id == line.product_id.id)
            print("order_id-------------",order_id)
            if order_id:
                order_id.quantity_done = line.quantity
                move_ids = order_id._action_confirm()
                move_ids._action_confirm()
        browse_obj.action_done()
        browse_obj.button_validate()
        res = super(CustomActionInvoice, self).action_invoice_open()
        return res
